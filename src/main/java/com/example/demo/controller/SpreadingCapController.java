package com.example.demo.controller;

import com.example.demo.model.dto.SpreadingResponse;
import com.example.demo.model.dto.StudentInformationResponse;
import com.example.demo.model.entities.Department;
import com.example.demo.model.entities.StudentSpreads;
import com.example.demo.service.SpreadingCapService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api/spreading")
public class SpreadingCapController {

    @Autowired
    SpreadingCapService spreadingCapService;

    @GetMapping
    public SpreadingResponse add(@RequestParam String fullName, @RequestParam(required = false) Department expectedDepartment) {
        return spreadingCapService.spread(fullName, expectedDepartment);
    }

    @GetMapping("/list")
    public List<StudentInformationResponse> getAllStudents() {
        return spreadingCapService.getAllSpreadings();
    }


}
