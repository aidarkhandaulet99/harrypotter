package com.example.demo.controller;

import com.example.demo.model.entities.Student;
import com.example.demo.service.StudentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.repository.query.Param;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api/students")
public class StudentController {
    //crud here

    private final StudentService studentService;


    @Autowired
    public StudentController(StudentService studentService) {
        this.studentService = studentService;
    }


    @GetMapping
    public List<Student> getAll(@RequestParam(required = false) Long wantType,
                                @RequestParam(required = false) Boolean deleted) {
        return studentService.getAll(wantType, deleted);
    }

    @PostMapping
    public void saveStudent(@RequestBody Student students) {
        studentService.save(students);
    }

    @DeleteMapping("/{id}")
    public void deleteStudent(@PathVariable Long id) {
        studentService.deleteById(id);
    }

}
