package com.example.demo.service;

import com.example.demo.model.*;
import com.example.demo.model.dto.SpreadingResponse;
import com.example.demo.model.dto.StudentInformationResponse;
import com.example.demo.model.entities.*;
import com.example.demo.model.entities.Character;
import com.example.demo.repository.StudentSpreadsRepository;
import com.example.demo.utils.Constants;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;


import java.util.ArrayList;
import java.util.List;

@Service
public class SpreadingCapService {

    BloodLineService bloodLineService;
    MagicWantService magicWantService;
    CharacterService characterService;
    DepartmentService departmentService;
    StudentSpreadsRepository studentSpreadsRepository;
    StudentService studentService;

    @Autowired
    public SpreadingCapService(BloodLineService bloodLineService,
                               MagicWantService magicWantService,
                               CharacterService characterService,
                               DepartmentService departmentService,
                               StudentSpreadsRepository studentSpreadsRepository,
                               StudentService studentService) {
        this.bloodLineService = bloodLineService;
        this.magicWantService = magicWantService;
        this.characterService = characterService;
        this.departmentService = departmentService;
        this.studentSpreadsRepository = studentSpreadsRepository;
        this.studentService = studentService;
    }

    public SpreadingResponse spread(String fullName, Department expectedDepartment) {
        Student student = studentService.getStudentByName(fullName);
        if (studentSpreadsRepository.existsByStudent(student)) {
            throw new RuntimeException("You cannot spread student second time!");
        }

        BloodLine bloodLine = bloodLineService.findById(student.getBloodLineId());
        WandType wandType = magicWantService.findById(student.getWandTypeId());
        Character character = characterService.findById(student.getCharacterId());

        DepartmentExpectations expectationByCharacteristics = departmentService.getExpectationByCharacteristics(character);
        DepartmentExpectations expectationByWand = departmentService.getExpectationByWand(wandType);
        DepartmentExpectations expectationByBloodLine = departmentService.getExpectationByBloodLine(bloodLine);

        DepartmentExpectations finalDepartmentExpectations = new DepartmentExpectations();

        finalDepartmentExpectations.gryffindorPercent = expectationByCharacteristics.gryffindorPercent
                + expectationByWand.gryffindorPercent
                + expectationByBloodLine.gryffindorPercent;

        finalDepartmentExpectations.slyzerinePercent = expectationByCharacteristics.slyzerinePercent
                + expectationByWand.slyzerinePercent
                + expectationByBloodLine.slyzerinePercent;

        finalDepartmentExpectations.ravenclawPercent = expectationByCharacteristics.ravenclawPercent
                + expectationByWand.ravenclawPercent
                + expectationByBloodLine.ravenclawPercent;

        finalDepartmentExpectations.hafflepuffPercent = expectationByCharacteristics.hafflepuffPercent
                + expectationByWand.hafflepuffPercent
                + expectationByBloodLine.hafflepuffPercent;

        String finalDecision = Constants.SLYTHERIN;
        if (expectedDepartment != null) {
            if (finalDepartmentExpectations.gryffindorPercent > 30 && Constants.GRYFFINDOR.equals(expectedDepartment.getDepartmentName())) {
                finalDecision = Constants.GRYFFINDOR;
            }

            if (finalDepartmentExpectations.slyzerinePercent > 30 && Constants.SLYTHERIN.equals(expectedDepartment.getDepartmentName())) {
                finalDecision = Constants.SLYTHERIN;
            }

            if (finalDepartmentExpectations.hafflepuffPercent > 30 && Constants.HUFFLEPUFF.equals(expectedDepartment.getDepartmentName())) {
                finalDecision = Constants.HUFFLEPUFF;
            }

            if (finalDepartmentExpectations.ravenclawPercent > 30 && Constants.RAVENCLAW.equals(expectedDepartment.getDepartmentName())) {
                finalDecision = Constants.RAVENCLAW;
            }
        } else {
            if (finalDepartmentExpectations.gryffindorPercent > finalDepartmentExpectations.ravenclawPercent
                    && finalDepartmentExpectations.gryffindorPercent > finalDepartmentExpectations.hafflepuffPercent
                    && finalDepartmentExpectations.gryffindorPercent > finalDepartmentExpectations.slyzerinePercent) {
                finalDecision = Constants.GRYFFINDOR;
            } else if  (finalDepartmentExpectations.slyzerinePercent > finalDepartmentExpectations.ravenclawPercent
                    && finalDepartmentExpectations.slyzerinePercent > finalDepartmentExpectations.hafflepuffPercent
                    && finalDepartmentExpectations.slyzerinePercent > finalDepartmentExpectations.gryffindorPercent) {
                finalDecision = Constants.SLYTHERIN;
            } else if (finalDepartmentExpectations.ravenclawPercent > finalDepartmentExpectations.slyzerinePercent
                    && finalDepartmentExpectations.ravenclawPercent > finalDepartmentExpectations.hafflepuffPercent
                    && finalDepartmentExpectations.ravenclawPercent > finalDepartmentExpectations.gryffindorPercent) {
                finalDecision = Constants.RAVENCLAW;
            } else {
                finalDecision = Constants.HUFFLEPUFF;
            }
        }

        StudentSpreads studentSpreads = new StudentSpreads();
        studentSpreads.student = studentService.getStudentByName(fullName);
        studentSpreads.departmentId = departmentService.getDepartmentByName(finalDecision).getId();
        studentSpreadsRepository.save(studentSpreads);

        return SpreadingResponse.of(finalDecision);
    }

    public List<StudentInformationResponse> getAllSpreadings() {
        List<StudentInformationResponse> studentInformationResponse = new ArrayList<>();
        List<StudentSpreads> studentSpreads = studentSpreadsRepository.findAll();

        for (StudentSpreads stspr: studentSpreads) {
            StudentInformationResponse r = new StudentInformationResponse();
            r.departmentName = departmentService.getDepartmentById(stspr.getDepartmentId()).getDepartmentName();
            r.studentName = stspr.student.getFullName();
            studentInformationResponse.add(r);
        }

        return studentInformationResponse;
    }


}
