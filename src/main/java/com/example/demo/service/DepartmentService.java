package com.example.demo.service;

import com.example.demo.model.DepartmentExpectations;
import com.example.demo.model.entities.*;
import com.example.demo.model.entities.Character;
import com.example.demo.repository.DepartmentRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Set;

import static com.example.demo.utils.Constants.*;

@Service
public class DepartmentService {

    @Autowired
    BloodLineService bloodLineService;

    @Autowired
    DepartmentRepository departmentRepository;

    @Autowired
    MagicWantService magicWantService;

    @Autowired
    CharacterService characterService;


    DepartmentExpectations getExpectationByWand(WandType wandType) {
        DepartmentExpectations departmentExpectations = new DepartmentExpectations();
        Set<WandTypeSpread> wandTypeSpreadSet = wandType.getWandTypeSpread();
        for (WandTypeSpread wandTypeSpread : wandTypeSpreadSet) {
            if (wandTypeSpread.getDepartment().getDepartmentName().equals(GRYFFINDOR)) {
                departmentExpectations.gryffindorPercent = wandTypeSpread.getPercent();
            } else if (wandTypeSpread.getDepartment().getDepartmentName().equals(HUFFLEPUFF)) {
                departmentExpectations.hafflepuffPercent = wandTypeSpread.getPercent();

            } else if (wandTypeSpread.getDepartment().getDepartmentName().equals(RAVENCLAW)) {
                departmentExpectations.ravenclawPercent = wandTypeSpread.getPercent();

            } else if (wandTypeSpread.getDepartment().getDepartmentName().equals(SLYTHERIN)) {
                departmentExpectations.slyzerinePercent = wandTypeSpread.getPercent();

            } else {
                throw new RuntimeException("Cannot parse department of wandSpread");
            }



        }
        return departmentExpectations;
    }



    DepartmentExpectations getExpectationByBloodLine(BloodLine bloodLine) {
        DepartmentExpectations departmentExpectations = new DepartmentExpectations();
        Set<BloodLineSpread> bloodLineSpreadSet = bloodLine.getBloodLineSpread();
        for (BloodLineSpread bloodLineSpread : bloodLineSpreadSet) {
            if(bloodLineSpread.getDepartment().getDepartmentName().equals(GRYFFINDOR)) {
                departmentExpectations.gryffindorPercent = bloodLineSpread.getPercent();
            } else if (bloodLineSpread.getDepartment().getDepartmentName().equals(HUFFLEPUFF)) {
                departmentExpectations.hafflepuffPercent = bloodLineSpread.getPercent();

            } else if (bloodLineSpread.getDepartment().getDepartmentName().equals(RAVENCLAW)) {
                    departmentExpectations.ravenclawPercent = bloodLineSpread.getPercent();

            } else if (bloodLineSpread.getDepartment().getDepartmentName().equals(SLYTHERIN)) {
                        departmentExpectations.slyzerinePercent = bloodLineSpread.getPercent();

            } else {
                System.out.println("something is wrong!");
            }
        }

        return departmentExpectations;
    }


    public DepartmentExpectations getExpectationByCharacteristics(Character character) {
        DepartmentExpectations departmentExpectations = new DepartmentExpectations();
        Set<CharacterSpread> characterSpreadSet = character.getCharacterSpread();
        for (CharacterSpread characterSpread : characterSpreadSet) {
            if (characterSpread.getDepartment().getDepartmentName().equals(GRYFFINDOR)) {
                departmentExpectations.gryffindorPercent = characterSpread.getPercent();
            } else if (characterSpread.getDepartment().getDepartmentName().equals(HUFFLEPUFF)) {
                departmentExpectations.hafflepuffPercent = characterSpread.getPercent();

            } else if (characterSpread.getDepartment().getDepartmentName().equals(RAVENCLAW)) {
                departmentExpectations.ravenclawPercent = characterSpread.getPercent();

            } else if (characterSpread.getDepartment().getDepartmentName().equals(SLYTHERIN)) {
                departmentExpectations.slyzerinePercent = characterSpread.getPercent();

            } else {
                System.out.println("something is wrong!");
            }



        }
        return departmentExpectations;
    }

    public Department getDepartmentByName(String finalDecision) {
        return departmentRepository.findByDepartmentName(finalDecision)
                .orElseThrow(() ->  new IllegalArgumentException("Cannot find department by Id : " + finalDecision));
    }

    public Department getDepartmentById(Long departmentId) {
        return departmentRepository.findById(departmentId)
                .orElseThrow(() ->  new IllegalArgumentException("Cannot find department by Id : " + departmentId));

    }
}
