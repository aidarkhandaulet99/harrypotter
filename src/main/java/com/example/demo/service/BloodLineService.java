package com.example.demo.service;

import com.example.demo.model.entities.BloodLine;
import com.example.demo.model.entities.Student;

import com.example.demo.repository.BloodLineRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class BloodLineService {

    @Autowired
    BloodLineRepository bloodLineRepository;

    public BloodLine findById(Long id) {
        return bloodLineRepository.findById(id)
                .orElseThrow(() ->  new IllegalArgumentException("Cannot find bloodLine by Id : " + id));
    }
}
