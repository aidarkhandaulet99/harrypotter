package com.example.demo.service;

import com.example.demo.model.entities.Student;
import com.example.demo.repository.StudentRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class StudentService {

    private final StudentRepository studentRepository;

    @Autowired
    public StudentService(StudentRepository studentRepository) {
        this.studentRepository = studentRepository;
    }

    public List<Student> getAll(Long wantType, Boolean deleted) {
//        //TODO fix me to make it sql query
//        return studentRepository.findAll().stream()
//                .filter(cc -> {
//                    if (deleted != null && wantType != null) {
//                        return deleted.equals(cc.getDeleted()) && wantType.equals(cc.getWandTypeId());
//                    } else if (deleted != null) {
//                        return deleted.equals(cc.getDeleted());
//                    } else if (wantType != null) {
//                        return wantType.equals(wantType);
//                    } else return true;
//                }).collect(Collectors.toList());

//        return studentRepository.findAllByParams(deleted, wantType);

        return studentRepository.findByQustomQuery(deleted);
    }

    public void save(Student students) {
        studentRepository.save(students);
    }

    public void deleteById(Long id) {
        studentRepository.deleteById(id);
    }

    public Student getStudentByName(String fullName) {
        Student student = studentRepository.findByFullName(fullName);
        if(student == null) {
            throw new RuntimeException("Student by name : " + fullName + " not found!");
        }
        return student;
    }
}
