package com.example.demo.repository;

import com.example.demo.model.entities.WandType;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

public interface WandTypeRepository extends JpaRepository<WandType,Long> {
    WandType findByName(String name);
    Optional<WandType> findById(Long id);

}
