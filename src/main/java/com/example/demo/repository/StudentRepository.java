package com.example.demo.repository;

import com.example.demo.model.entities.Student;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;

public interface StudentRepository extends JpaRepository<Student, Long> {
    Student findByFullName(String fullName);

    @Query("SELECT s FROM Student s " +
            "WHERE (:deleted is null or s.deleted = :deleted) AND (:wantTypeId is null or s.wandTypeId = :wantTypeId)")
    List<Student> findAllByParams(Boolean deleted, Long wantTypeId);

    @Query(value = "SELECT * FROM students WHERE :deleted is null or is_deleted = :deleted", nativeQuery = true)
    List<Student> findByQustomQuery(Boolean deleted);
}
