package com.example.demo.model.entities;

import javax.persistence.*;

@Entity
@Table(name = "students")
public class Student {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private String fullName;



    private Long bloodLineId;
    private Long wandTypeId;

    private Long characterId;

    @Column(name = "is_deleted")
    private Boolean deleted;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getFullName() {
        return fullName;
    }

    public void setFullName(String fullName) {
        this.fullName = fullName;
    }



    public Long getWandTypeId() {
        return wandTypeId;
    }

    public void setWandTypeId(Long wandTypeId) {
        this.wandTypeId = wandTypeId;
    }

    public Long getBloodLineId() {
        return bloodLineId;
    }


    public void setBloodLineId(Long bloodLineId) {
        this.bloodLineId = bloodLineId;
    }

    public Long getCharacterId() {
        return characterId;
    }

    public void setCharacterId(Long characterId) {
        this.characterId = characterId;
    }


    public Boolean getDeleted() {
        return deleted;
    }

    public void setDeleted(Boolean deleted) {
        this.deleted = deleted;
    }
}
