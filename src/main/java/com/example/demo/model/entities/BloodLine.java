package com.example.demo.model.entities;

import javax.persistence.*;
import java.util.Set;

@Entity
@Table(name = "blood_line")
public class BloodLine {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @OneToMany(mappedBy = "bloodLine")
    private Set<BloodLineSpread> bloodLineSpread;



    private String bloodLineName;



    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getBloodLineName() {
        return bloodLineName;
    }

    public void setBloodLineName(String bloodLineName) {
        this.bloodLineName = bloodLineName;
    }


    public Set<BloodLineSpread> getBloodLineSpread() {
        return bloodLineSpread;
    }

    public void setBloodLineSpread(Set<BloodLineSpread> bloodLineSpread) {
        this.bloodLineSpread = bloodLineSpread;
    }

}
