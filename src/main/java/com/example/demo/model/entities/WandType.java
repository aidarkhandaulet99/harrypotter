package com.example.demo.model.entities;

import javax.persistence.*;
import java.util.Set;

@Entity
@Table(name="wand_type")
public class WandType {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @OneToMany(mappedBy = "wandType")
    private Set<WandTypeSpread> wandTypeSpread;

    private String name;

    public Set<WandTypeSpread> getWandTypeSpread() {
        return wandTypeSpread;
    }

    public void setWandTypeSpread(Set<WandTypeSpread> wandTypeSpread) {
        this.wandTypeSpread = wandTypeSpread;
    }



    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
