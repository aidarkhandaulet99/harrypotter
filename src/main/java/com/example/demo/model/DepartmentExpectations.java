package com.example.demo.model;

public class DepartmentExpectations {
    public double gryffindorPercent;
    public double hafflepuffPercent;
    public double ravenclawPercent;
    public double slyzerinePercent;

    @Override
    public String toString() {
        return "DepartmentExpectations{" +
                ", gryffindorPercent=" + gryffindorPercent +
                ", hafflepuffPercent=" + hafflepuffPercent +
                ", ravenclawPercent=" + ravenclawPercent +
                ", slyzerinePercent=" + slyzerinePercent +
                '}';
    }
}
