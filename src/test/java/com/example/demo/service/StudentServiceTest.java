package com.example.demo.service;

import com.example.demo.model.entities.Student;
import com.example.demo.repository.StudentRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.List;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
class StudentServiceTest {
    @Mock
    StudentRepository studentRepository;

    StudentService studentService;

    @BeforeEach
    void setUp() {
        studentService = new StudentService(studentRepository);
    }

    @Test
    void getAll() {
        //given
        Long wantType = 1L;
        Boolean deleted = true;

        Student student1 = new Student();
        Student student2 = new Student();
        Student student3 = new Student();

        //when
        when(studentRepository.findByQustomQuery(deleted)).thenReturn(List.of(student1, student2, student3));

        //then
        List<Student> all = studentService.getAll(wantType, deleted);
        assertEquals(3, all.size());
    }
}